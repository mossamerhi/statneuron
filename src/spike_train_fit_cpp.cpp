#include <Rcpp.h>
using namespace Rcpp;


//' History encoding for renewal process
//'
//' Create a history encoding matrix with only one event in each row. This corresponds to a renewal process.
//'
//' @param spike_train_ms Spike train times in ms.
//' @param history_span No. of time points, i.e. column of history matrix
//'
//' @return history encoding matrix
//'
//' @examples
//'
//' @export
// [[Rcpp::export]]
NumericMatrix renewal_history_encoding(List spike_train_ms, int history_span) {

  NumericMatrix history_matrix;

  return(history_matrix);
}

//' Izhikevich simulation
//'
//' spike_train_fit_cpp() simulates the Izhikevich dynamical model using the parameters given
//'
//' The ODEs are solved with an Euler type Finite Difference scheme.
//'
//' @param a,b,c,d,I0 numeric(s), izhikevich parameters
//' @param delta_t numeric, in milliseconds, stepsize in time
//' @param input_sd The variance of the input current
//' @param max_time Length of simulation in milliseconds
//'
//' @return list of simulated processes and input current
//'
//' @export
// [[Rcpp::export]]
Rcpp::List spike_train_fit_cpp(
    double a,
    double b,
    double c,
    double d,
    double I0,
    double delta_t,
    double input_sd,
    double max_time) {
  int M = (int)floorf(max_time / delta_t);

  // Output all parameters
  List params = Rcpp::List::create(
    Named("a") = a,
    Named("b") = b,
    Named("c") = c,
    Named("d") = d,
    Named("I0") = I0,
    Named("delta_t") =  delta_t,
    Named("input_sd") = input_sd,
    Named("max_time") = max_time,
    _["M"] = M); //no. of time points simulated

  NumericVector ts(M);
  //Rprintf("M = %i.\n", M);

  NumericVector I_ts(M), u_ts(M), v_ts(M);
  I_ts = I0 + input_sd * rnorm(M, 0, 1);
  List spike_train = List();

  // initialise u and v
  double v = -70;
  double u = b * v;

  // Rprintf("v = %.f\nu = %.f\n", v,u);

  for(int t_index = 0; t_index < M; ++t_index){
    ts[t_index] = (t_index+1)*delta_t;

    // numeric integration: 1st forward euler
    v =
      v + delta_t * (.04 * pow(v, 2) + 5 * v + 140 - u + I_ts[t_index]);
    u = u + delta_t * a * (b * v - u);

    if (30 <= v) {
      int spike_t_index = t_index + 1;
      double spike_t_ms = ts[t_index];

      v = c;
      u = u + d;

      // save spike
      spike_train.push_back(
        List::create(
          Named("t_index") = spike_t_index,
          Named("spike_ms") = spike_t_ms));
    }
    // save iteration
    v_ts[t_index] = v;
    u_ts[t_index] = u;
  }
  return(
    List::create(_["input_current"] = I_ts,
                 _["v"] = v_ts,
                 _["u"] = u_ts,
                 _["spike_train"] = spike_train,
                 _["time"] = ts,
                 _["params"] = params)
  );
}

// [[Rcpp::export]]
Rcpp::List spike_train_fit_cpp_long(
    double a,
    double b,
    NumericVector cc,
    NumericVector dd,
    double I0,
    double delta_t,
    double input_sd) {
  // int M = (int)floorf(max_time / delta_t);

  if ( cc.length() != dd.length() ) {
    stop("The cc and dd parameters must be of equal length.");
  }

  int M = cc.length();
  double max_time = M * delta_t;

  // Output all parameters
  List params = Rcpp::List::create(
    Named("a") = a,
    Named("b") = b,
    Named("c") = cc,
    Named("d") = dd,
    Named("I0") = I0,
    Named("delta_t") =  delta_t,
    Named("input_sd") = input_sd,
    Named("max_time") = max_time,
    _["M"] = M); //no. of time points simulated

  NumericVector ts(M);
  //Rprintf("M = %i.\n", M);

  NumericVector I_ts(M),u_ts(M), v_ts(M);
  I_ts = I0 + input_sd * rnorm(M, 0, 1);
  List spike_train = List();

  // initialise u and v
  double v = -70;
  double u = b * v;

  // Rprintf("v = %.f\nu = %.f\n", v,u);

  for(int t_index = 0; t_index < M; ++t_index){
    double c = cc[t_index];
    double d = dd[t_index];

    ts[t_index] = (t_index+1)*delta_t;

    // numeric integration: 1st forward euler
    v =
      v + delta_t * (.04 * pow(v, 2) + 5 * v + 140 - u + I_ts[t_index]);
    u = u + delta_t * a * (b * v - u);

    if (30 <= v) {
      int spike_t_index = t_index + 1;
      double spike_t_ms = ts[t_index];

      v = c;
      u = u + d;

      // save spike
      spike_train.push_back(
        List::create(
          Named("t_index") = spike_t_index,
          Named("spike_ms") = spike_t_ms));
    }
    // save iteration
    v_ts[t_index] = v;
    u_ts[t_index] = u;
  }
  return(
    List::create(_["input_current"] = I_ts,
                 _["v"]             = v_ts,
                 _["u"]             = u_ts,
                 _["spike_train"]   = spike_train,
                 _["time"]          = ts,
                 _["params"]        = params)
  );
}
