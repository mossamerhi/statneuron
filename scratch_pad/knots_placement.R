

library(tidyverse)
library(magrittr)

library(patchwork)
library(glue)

theme_set(ggpubr::theme_classic2())

ts_sample <-
  spike_train("TS") %>%
  simulate(delta_t = .1, max_time = 20e3 + 80, seed = 1234, input_sd = 5) %>%
  helper_remove_burn_in(80)


ts_mean_point <- ts_sample$spike_train$spike_ms %>% {c(.[[1]], diff(.))} %>% mean()

specific_knots <-
  knots_around_spikes(1000, .1, ts_mean_point, interval_length = .35, no_of_intervals = 22)


plot_knots(ts_sample, specific_knots)

specific_knots_with_boundary <-
  specific_knots %>%
  knots_add_boundaries(timestep=.1, history_span = 1000)


plot_knots(ts_sample, specific_knots_with_boundary)

ts_sample_design <- ts_sample %>% helper_binary_spike_train(params = 1000)
uniform_knots_grid <- 1000/c(10, 25, 50, 100, 250, 500)

# uniform_knots <- knots_on_uniform(ts_sample_design, uniform_knots_grid[[1]])
uniform_knots_df <-
  data_frame(window_length = uniform_knots_grid) %>%
  mutate(uniform_knots =
           pmap(., function(...) knots_on_uniform(ts_sample_design,...)))


uniform_knots_df %<>%
  mutate(
    plot_knots_uniform = map(uniform_knots, ~ plot_knots(ts_sample, .))
  )


all_uniform_knots_plots <- uniform_knots_df$plot_knots_uniform %>%
  wrap_plots(nrow = 2)

all_uniform_knots_plots <-  all_uniform_knots_plots * (
  theme(legend.position = "none",
        title = element_blank(),
        axis.title.x = element_blank()))

all_uniform_knots_plots +
  # labs(caption = "Dude")
         # paste0("Width: ", knitr::combine_words(uniform_knots_grid)))

ggsave(
  filename = "scratch_pad/knots_uniform_placement_tonic_spiking.pdf",
  # plot = all_uniform_knots_plots,
  device = cairo_pdf,
  scale = 2.3,
  width = 7,
  height = 4,
  dpi = "print"
)



tb_sample <-
  spike_train("TB") %>%
  simulate(delta_t = .1, max_time = 20e3 + 80, seed = 1234, input_sd = 5) %>%
  helper_remove_burn_in(80)

tb_sample %>% hist

plotly::ggplotly()

tb_knots <- knots_around_spikes(
  history_span = 1000,
  timestep = .1,
  modes_ms = c(4.33, 46.85),
  interval_length = c(.35, .35),
  no_of_intervals = c(9, 29),
  repeat_modes = FALSE
)

plot_knots(
  tb_sample,
  knots = tb_knots
) +
  labs(caption = glue("modes: ", knitr::combine_words(c(4.33, 46.85)))) +
  theme_bw() +
  theme(title = element_text(),
        legend.position = "bottom")

