

ts_sample <-
  spike_train("TS") %>%
  simulate(delta_t = .1, max_time = 20e3 + 80, seed = 1234, input_sd = 5) %>%
  helper_remove_burn_in(80)

tb_sample <-
  spike_train("TB") %>%
  simulate(delta_t = .1, max_time = 20e3 + 80, seed = 1234, input_sd = 5) %>%
  helper_remove_burn_in(80)

ts_mean_point <- ts_sample$spike_train$spike_ms %>% {c(.[[1]], diff(.))} %>% mean()
ts_knots <-
  knots_around_spikes(1000, .1, ts_mean_point, interval_length = .35, no_of_intervals = 22) %>%
  knots_add_boundaries(timestep=.1, history_span = 1000)

tb_knots <- knots_around_spikes(
  history_span = 1000,
  timestep = .1,
  modes_ms = c(4.33, 46.85),
  interval_length = c(.35, .35),
  no_of_intervals = c(9, 29),
  repeat_modes = FALSE) %>%
  knots_add_boundaries(timestep = .1, history_span = 1000)


ts_indicator <-
  createSpikeTrainBasisMatrix(timestep = .1, history_span = 1000,
                              knots = ts_knots,
                              degree = 0L, intercept = TRUE)
ts_spline <-
  createSpikeTrainBasisMatrix(timestep = .1, history_span = 1000,
                              knots = ts_knots,
                              degree = 3L, intercept = TRUE)

tb_indicator <-
  createSpikeTrainBasisMatrix(timestep = .1, history_span = 1000,
                              knots = tb_knots,
                              degree = 0L, intercept = TRUE)
tb_spline <-
  createSpikeTrainBasisMatrix(timestep = .1, history_span = 1000,
                              knots = tb_knots,
                              degree = 3L, intercept = TRUE)

ts_sample_design <- ts_sample %>% helper_binary_spike_train(1000)
tb_sample_design <- tb_sample %>% helper_binary_spike_train(1000)

ts_sample_histogram <- ts_sample %>% hist()
ts_sample_histogram <-
  ts_sample_histogram +
  theme(legend.position = "none",
        title = element_blank()) +
  xlim(0,100)
tb_sample_histogram <- tb_sample %>% hist()
tb_sample_histogram <-
  tb_sample_histogram +
  theme(legend.position = "none",
        title = element_blank()) +
  xlim(0,100)


ts_sample_histogram +
  plot_splines(
    bSplineBasis = ts_indicator,
    knots = ts_knots,
    Boundary.knots = NULL,
    history_time = ts_sample_design$history_time
  ) +
  plot_layout(ncol = 1)


ggsave(
  filename = "scratch_pad/best_knots_indicator_tonic_spike.pdf",
  device = cairo_pdf,
  scale = 2.1,
  width = 9,
  height = 6
)


ts_sample_histogram +
  plot_splines(
    bSplineBasis = ts_spline,
    knots = ts_knots,
    Boundary.knots = NULL,
    history_time = ts_sample_design$history_time
  ) +
  plot_layout(ncol = 1)


ggsave(
  filename = "scratch_pad/best_knots_splines_tonic_spiking.pdf",
  device = cairo_pdf,
  scale = 2.1,
  width = 9,
  height = 6
)

tb_sample_histogram +
  plot_splines(
    bSplineBasis = tb_indicator,
    knots = tb_knots,
    Boundary.knots = NULL,
    history_time = tb_sample_design$history_time
  ) +
  plot_layout(ncol = 1)


ggsave(
  filename = "scratch_pad/best_knots_indicator_tonic_bursting.pdf",
  device = cairo_pdf,
  scale = 2.1,
  width = 9,
  height = 6
)



tb_sample_histogram +
  plot_splines(
    bSplineBasis = tb_spline,
    knots = tb_knots,
    Boundary.knots = NULL,
    history_time = tb_sample_design$history_time
  ) +
  plot_layout(ncol = 1)

ggsave(
  filename = "scratch_pad/best_knots_splines_tonic_bursting.pdf",
  device = cairo_pdf,
  scale = 2.1,
  width = 9,
  height = 6
)
