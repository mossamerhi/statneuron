

library(statneuron)
library(tidyverse)


st_sample <-
  spike_train("TS") %>%
  simulate(delta_t = .1,
           max_time = 20e3,
           input_sd  = 5)

helper_interspike_intervals <- function(object) {
  stopifnot(
    hasName(object, "spike_train")
  )
  object$spike_train %>%
    dplyr::mutate(
      interspike_interval_ms = c(spike_ms[1], diff(spike_ms))
    )
}


# st_sample %>%
#   helper_interspike_intervals()

st_sample %>%
  helper_interspike_intervals() %>%

  ggplot(aes(spike_ms, interspike_interval_ms)) +
  geom_line() +
  geom_point(size = .3) +

  scale_x_log10()



st_samples <-
  crossing(neuron_type = c("TS", "TB", "MM")) %>%
  mutate(neurons = pmap(.,
       function(neuron_type)
         spike_train(neuron_type) %>%
         simulate(delta_t = .1, max_time = 20e3, input_sd = 5, nsim = 10)
  ))

st_samples <- st_samples %>%
  unnest() %>%
  rowid_to_column("sim_id") %>%
  mutate(spike_train = neurons %>%
           map(helper_interspike_intervals)) %>%
  unnest(spike_train)



st_burn_in_plot <- st_samples %>%

  ggplot(aes(spike_ms, interspike_interval_ms, color = factor(sim_id))) +

  geom_line(
    alpha = .2,
    show.legend = FALSE) +
  geom_point(size = .1, show.legend = FALSE) +

  facet_grid(neuron_type~., scales="free_y") +

  coord_trans(limx = c(0,1e3)) +

  geom_vline(xintercept = 80) +

  labs(x = "Spike train [ms]",
       y = "Interspike-intervals") +

  ggpubr::theme_classic2() +

  NULL

library(ggrepel)
st_burn_in_plot +
  labs(caption = "80 ms")

ggsave(
  # plot = st_burn_in_plot,
  filename = "scatch_pad/burn_in_TS_TB_MM_10_each_input_sd_5_label.pdf",
  device = cairo_pdf,
  dpi = "print",
  scale = 1.9,
  width = 7,
  height = 4
)

# plotly::ggplotly(.Last.value)

