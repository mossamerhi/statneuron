

uniform_knots_grid <- 1000/c(10, 25, 50, 100, 250, 500)
uniform_knots_df <-
  data_frame(window_length = uniform_knots_grid) %>%
  mutate(uniform_knots =
           pmap(., function(...) knots_on_uniform(ts_sample_design,...)))

